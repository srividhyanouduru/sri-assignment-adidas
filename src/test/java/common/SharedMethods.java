package common;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.junit.Assert;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Duration;


public class SharedMethods {

    private static WebElement elementDetails;
    private static long maxWait = 20;
    private static long nanoAdjustment = 1;

    /**
     * This method will set the Chrome driver property based on the OS
     */
    public WebDriver createChromeDriver() {
        WebDriver driver = null;
        if (SystemUtils.IS_OS_MAC) {
            System.setProperty("webdriver.chrome.driver", "src/test/resources/chromeDriver/chromedriverMac");
        } else if (SystemUtils.IS_OS_WINDOWS) {
            System.setProperty("webdriver.chrome.driver", "src/test/resources/chromeDriver/chromedriverWindows.exe");
        } else if (SystemUtils.IS_OS_LINUX) {
            System.setProperty("webdriver.chrome.driver", "src/test/resources/chromeDriver/chromedriverLinux");
        }
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");
        driver = new ChromeDriver(options);

        return driver;
    }

    /**
     * This method can be used to locate an element using different locators
     * and also implementing a dynamic wait
     * @param driver web driver
     * @param ByLocator     like Id or xpath or className or cssSelector
     * @param elementLocation in string format
     * @return WebElement
     */
    public static WebElement locateElement(WebDriver driver, String ByLocator, String elementLocation) {
        StringWriter sw = new StringWriter();
        try {
            switch (ByLocator.toLowerCase()) {
                case "id":
                    elementDetails = (new WebDriverWait(driver, Duration.ofSeconds(maxWait,nanoAdjustment))).until(ExpectedConditions.presenceOfElementLocated(By.id(elementLocation)));
                    break;
                case "xpath":
                    elementDetails = (new WebDriverWait(driver, Duration.ofSeconds(maxWait,nanoAdjustment))).until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementLocation)));
                    break;
                case "classname":
                    elementDetails = (new WebDriverWait(driver, Duration.ofSeconds(maxWait,nanoAdjustment))).until(ExpectedConditions.presenceOfElementLocated(By.className(elementLocation)));
                    break;
                case "tagname":
                    elementDetails = (new WebDriverWait(driver, Duration.ofSeconds(maxWait,nanoAdjustment))).until(ExpectedConditions.presenceOfElementLocated(By.tagName(elementLocation)));
                    break;
                case "css":
                    elementDetails = (new WebDriverWait(driver, Duration.ofSeconds(maxWait,nanoAdjustment))).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(elementLocation)));
                    break;
                case "name":
                    elementDetails = (new WebDriverWait(driver, Duration.ofSeconds(maxWait,nanoAdjustment))).until(ExpectedConditions.presenceOfElementLocated(By.name(elementLocation)));
                    break;
            }
        } catch(Exception ex) {
            ex.printStackTrace(new PrintWriter(sw));
            Assert.fail();
        }
        return elementDetails;
    }

}
