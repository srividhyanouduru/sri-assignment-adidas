package stepDefinitions;

import common.SharedMethods;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjectModel.DemoBlazeCartPage;
import pageObjectModel.DemoBlazeHomePage;
import pageObjectModel.DemoBlazeOrderPage;
import pageObjectModel.DemoBlazeProductsPage;

import java.time.Duration;


public class DemoBlazeSteps {

    private WebDriver driver;
    private WebDriverWait wait;

    // Opening chrome browser and navigating to DemoBlaze homepage
    @Given("^A user navigates to (.*)$")
    public  void navigate(String url) {
        SharedMethods common = new SharedMethods();
        driver = common.createChromeDriver();
        driver.navigate().to(url);
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, Duration.ofSeconds(5,1));
    }

    // Select's different type of product from  categories
    @When("^user select (.*) in categories$")
    public void typeOfProduct(String productType)
    {
        switch (productType) {
            case "Laptops":
                DemoBlazeHomePage.laptops(driver).click();
                break;
        // TODO: other product types like Mobiles can be added here
        }
    }

    @And("^user select (.*) in list of laptops$")
    public void selectLaptop(String laptopModel)
    {
        DemoBlazeProductsPage.selectLaptop(driver, laptopModel).click();
    }

    @And("^user adds the product to the cart$")
    public void addingToCart() {
        DemoBlazeCartPage.addToCart(driver).click();
        waitForAlert();
        driver.switchTo().alert().accept();
    }

    @And("^user navigates to home page$")
    public void homePage()
    {
        DemoBlazeHomePage.home(driver).click();
    }

    @And("^user navigates to cart page$")
    public void cartPage()
    {
        DemoBlazeCartPage.cart(driver).click();
    }


    @And("^user deletes (.*) from the cart$")
    public void deleteItemFromCart(String model)
    {
        DemoBlazeCartPage.deleteFromCart(driver, model).click();
    }

    @And("^user places the order$")
    public void placingOrder() throws InterruptedException {
        //TODO: below dynamic wait is not working properly, needs a fix to remove static thread sleep
      //  wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Place Order')]")));
        Thread.sleep(5000);
        DemoBlazeOrderPage.placeOrder(driver).click();
    }

    @And("^user fills personal details (.*), (.*), (.*), (.*), (.*), (.*)$")
    public void fillPersonalDetails(String name, String creditCard, String country, String city, String month, String year) throws InterruptedException {

        //TODO: below dynamic wait is not working properly, needs a fix to remove static thread sleep
        Thread.sleep(5000);
      //  Assert.assertTrue(DemoBlazeOrderPage.placeOrderForm(driver).isDisplayed());
        DemoBlazeOrderPage.name(driver).sendKeys(name);
        DemoBlazeOrderPage.creditCard(driver).sendKeys(creditCard);
        DemoBlazeOrderPage.country(driver).sendKeys(country);
        DemoBlazeOrderPage.city(driver).sendKeys(city);
        DemoBlazeOrderPage.month(driver).sendKeys(month);
        DemoBlazeOrderPage.year(driver).sendKeys(year);

    }

    @And("^user clicks purchase option$")
    public void userClicksPurchaseOption()
    {
        DemoBlazeOrderPage.purchase(driver).click();
    }

    @Then("^order id generated$")
    public void orderIdGenerated() {

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h2[contains(text(),'Thank you for your purchase!')]")));
        System.out.println("checking purchase confirmation ");

        Assert.assertTrue(DemoBlazeOrderPage.purchaseConfirmation(driver).isDisplayed());
        Assert.assertTrue(DemoBlazeOrderPage.orderDetails(driver).isDisplayed());
        System.out.println("Generated order details -> " + DemoBlazeOrderPage.orderDetails(driver).getText());

        driver.close();
    }

    private void waitForAlert(){
        wait.until(ExpectedConditions.alertIsPresent());
    }
}

