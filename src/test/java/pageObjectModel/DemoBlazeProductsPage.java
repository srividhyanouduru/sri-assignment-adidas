package pageObjectModel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DemoBlazeProductsPage {

    public static WebElement selectLaptop(WebDriver driver, String laptopModel){
        return common.SharedMethods.locateElement(driver, "xpath", "//a[contains(text(),'"+ laptopModel +"')]");
    }
}
