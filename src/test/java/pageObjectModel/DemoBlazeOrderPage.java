package pageObjectModel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DemoBlazeOrderPage {

    public static WebElement placeOrder(WebDriver driver){
        return common.SharedMethods.locateElement(driver, "xpath", "//button[contains(text(),'Place Order')]");
    }

    public static WebElement purchaseConfirmation(WebDriver driver){
        return common.SharedMethods.locateElement(driver, "xpath", "//h2[contains(text(),'Thank you for your purchase!')]");
    }

    public static WebElement placeOrderForm(WebDriver driver){
        return common.SharedMethods.locateElement(driver, "xpath", "//h5[contains(text(),'Place Order')]");
    }

    public static WebElement orderDetails(WebDriver driver){
        return common.SharedMethods.locateElement(driver, "xpath", "//p[contains(text(),'Id')]");
    }

    public static WebElement creditCard(WebDriver driver){
        return common.SharedMethods.locateElement(driver, "id", "card");
    }

    public static WebElement name(WebDriver driver){
        return common.SharedMethods.locateElement(driver, "id", "name");
    }

    public static WebElement country(WebDriver driver){
        return common.SharedMethods.locateElement(driver, "id", "country");
    }

    public static WebElement city(WebDriver driver){
        return common.SharedMethods.locateElement(driver, "id", "city");
    }

    public static WebElement month(WebDriver driver){
        return common.SharedMethods.locateElement(driver, "id", "month");
    }

    public static WebElement year(WebDriver driver){
        return common.SharedMethods.locateElement(driver, "id", "year");
    }

    public static WebElement purchase(WebDriver driver){
        return common.SharedMethods.locateElement(driver, "xpath", "//button[contains(text(),'Purchase')]");
    }
}
