package pageObjectModel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DemoBlazeHomePage {

    public static WebElement laptops(WebDriver driver){
        return common.SharedMethods.locateElement(driver, "xpath", "//a[contains(@onclick,'notebook')]");
    }

    public static WebElement home(WebDriver driver){
        return common.SharedMethods.locateElement(driver, "xpath", "//a[contains(text(),'Home')]");
    }
}
