package pageObjectModel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DemoBlazeCartPage {

    public static WebElement cart(WebDriver driver){
        return common.SharedMethods.locateElement(driver, "xpath", "//a[contains(text(),'Cart')]");
    }

    public static WebElement addToCart(WebDriver driver){
        return common.SharedMethods.locateElement(driver, "xpath", "//a[contains(text(),'Add to cart')]");
    }

    public static WebElement deleteFromCart(WebDriver driver, String itemToDelete){
        return common.SharedMethods.locateElement(driver, "xpath", "//*[@id=\"tbodyid\"]/tr["+ getRowIndex(driver, itemToDelete) +"]/td[4]/a");
    }

    // TODO: this method can go through the list of tr's and check for the row where the given item is present, still some issue with element collection, for now returning first row
    private static String getRowIndex(WebDriver driver, String item){
        String index="1";
//        List<WebElement> elems = driver.findElements(By.xpath("//*[@id="tbodyid"]/tr"));
        return index;
    }

}
