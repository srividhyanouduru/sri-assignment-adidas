package testRunner;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:output/report.html"}, features = {"src/test/resources/feature"}, glue = {"stepDefinitions"}, monochrome = true)
public class RunDemoTest {
}
