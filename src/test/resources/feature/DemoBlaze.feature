Feature: As a user I would like to purchase a laptop and verify the webflow functionality
  Background:
    Given A user navigates to https://www.demoblaze.com

  Scenario Outline: Verify adding to cart functionality
    When user select <typeOfProduct> in categories
    And user select <firstLaptopModel> in list of laptops
    And user adds the product to the cart
    # Then verify cart
    And user navigates to home page
    When user select <typeOfProduct> in categories
    And user select <secondLaptopModel> in list of laptops
    And user adds the product to the cart
    And user navigates to cart page
    And user deletes <secondLaptopModel> from the cart
    And user places the order
    And user fills personal details <name>, <creditCard>, <country>, <city>, <month>, <year>
    And user clicks purchase option
    Then order id generated
    # Then verify Order Id and log it
    Examples:
      | typeOfProduct | firstLaptopModel  | secondLaptopModel | name | creditCard | country     | city      | month | year |
      | Laptops       | Sony vaio i5      | Dell i7 8gb       | SV   | 123456     | Netherlands | Amsterdam | 08    | 2021 |



