# Assignment-Vidhya-Adidas

# README #

This README will explain the contents of the folder and process to run the test

### What is this repository for? ###

This respository covers the below test assignments for adidas QA role:
- UI Test automation assignment of DemoBlaze page
- API test automation of PetStore
- Test management assignment result for new project

### How do I understand the set up? ###



##### Project Setup:

Project contains the below set of folders:

 `src/test/java` 
 	
     Packages
     - common (Package with common methods which are reused across the project)
     - pageObjectModel (Pacakge with all elements located for different pages, which can be reused directly in the tests)
     - testRunner (Package with the main runner file, which needs to be exeucted to run the whole test)
     - stepDefinitions (Package where the actual test steps are implemented)
     
     
`src/test/resources`
	
	Sub-Folders
	- chromeDriver (Folder with ChromeDrivers for Mac/Linux/Windows so that the test can run on all 3 platforms, please note: I ran the test on MacBook)
	- feature (Feature file created with Gherkin language)
	     
`PetStoreAPITestAssignment`
	
	- Sri-Addidas-PetStore-Test.postman_collection.json (API tests for PetStore project)
		-	Please note: Petstore implementation has a load balances it seems where calls are going to alternate servers, POST call will create item only on 1 server and POST has to be called twice 
	- Above json file can be imported to Postman and run the collection using options(PetStorePostmanTest-RunOptions.png)
	
`TestManagementAssignment`
	
	- Test Management Assignment document has the answers per section

##### Technologies used:
Below list of technologies are used for UI and API automation:

	Cucumber
	Gherkin
	Selenium
	Java
	Maven
	Junit Framework
	Postman


##### Automation Test covers :

This automation test suite will use Features file which covers the test scenarios described in the assignment

##### How to run this automation test :

Steps to be followed in order to run this test suite:
	
	Step 1: Clone this repository using git clone:
	Step 2: Use IntelliJ(Or similar tool) to open the project pulled to your workstation
	Step 3: Open 'RunDemoTest.java' file located in 'src/test/java/testRunner' and run the file
	Step 4: Report will be generated at rootDirectory/output/report.html(open this file in browser to see the result)
																		
	
##### Required Software/tools:
* Java 8
* IntelliJ
* SourceTree (Or similar tool)
* BitBucket 

##### Assumptions or Bugs found:
- API automation has an extra GET call after POST due to the bug of calls going to different servers(may be loadbalancer). Item is retreived on alternate GET calls
- Couple of Static waits are still there in UI automation project, dynamic wait setup is in place but there are some issues while using it, couldnt manage to finish this within given time.
